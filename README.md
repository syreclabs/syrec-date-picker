# Cordova DatePicker plugin (iOS and Android)

This is heavily modified version of https://github.com/VitaliiBlagodir/cordova-plugin-datepicker
with unified API and 'datetime' mode support on both iOS and Android.

## Installation

```bash
cordova plugin add https://bitbucket.org/syreclabs/syrec-date-picker.git
```

## Usage

```js
var options = {
  date: new Date(),
  mode: 'date'
};

window.cordova.plugins.DatePicker.show(options, function(date) {
  alert("date result " + date);  
});
```

## Options

### mode
The mode of the date picker.

Type: String

Values: `date` | `time` | `datetime`

Default: `date`

### date
Selected date.

Type: Date | timestamp | ISO 8601 string

Default: `new Date()`

### minDate
Minimum date.

Type: Date | timestamp | ISO 8601 string

Default: `null`

### maxDate
Maximum date.

Type: Date | timestamp | ISO 8601 string

Default: `null` 

### doneButtonLabel
Label of done button.

Typ: String

Default: `Done`

### doneButtonColor
Done button tint color.

Typ: String

Default: `#0000FF`

### cancelButtonLabel
Label of cancel button.

Type: String

Default: `Cancel`

### cancelButtonColor - iOS
Cancel button tint color.

Type: String

Default: `#000000`

### x - iOS (iPad only)
X position of date picker. The position is absolute to the root view of the application.

Type: String

Default: `0`

### y - iOS (iPad only)
Y position of date picker. The position is absolute to the root view of the application.

Type: String

Default: `0`

## Requirements
- Cordova > =3.0
- Android >= 3.0
- iOS >= 5.0

## Example

```js
var options = {
  date: new Date(),
  mode: 'date'
};

window.cordova.plugins.DatePicker.show(options, function(date){
  alert("date result " + date);  
});
```
