/**
 * Cordova DatePicker Plugin
 * https://github.com/syreclabs/syrec-plugins-datepicker.git
 */

package io.syrec.datepicker;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;
import au.com.silkhospitality.Silk_testing.R;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class SyrecDatePicker extends CordovaPlugin {

    private static final String MODE_DATE = "date";
    private static final String MODE_TIME = "time";
    private static final String MODE_DATETIME = "datetime";
    private static final String TAG = "SyrecDatePicker";

    private DateFormat _dateFormat = null;
    private CallbackContext _callbackContext = null;
    private String _mode = null;
    private Calendar _dateCalendar = null;
    private Calendar _minDateCalendar = null;
    private Calendar _maxDateCalendar = null;
    private String _doneButtonLabel = null;
    private String _cancelButtonLabel = null;
    private boolean _cancelled = false;

    @Override
    public boolean execute(final String action, final JSONArray data, final CallbackContext callbackContext) {
        try {
            // we're passing dates formatted with toISOString() which always return dates without timezones
            // see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toISOString
            _dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'");
            _dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

            final JSONObject obj = data.getJSONObject(0);
            _mode = obj.getString("mode");

            final String dateString = obj.getString("date");
            _dateCalendar = GregorianCalendar.getInstance();
            _dateCalendar.setTime(_dateFormat.parse(dateString));

            final String minDateString = obj.isNull("minDate") ? null : obj.getString("minDate");
            if (minDateString != null && minDateString.length() > 0) {
                _minDateCalendar = GregorianCalendar.getInstance();
                _minDateCalendar.setTime(_dateFormat.parse(minDateString));
            }

            final String maxDateString = obj.isNull("maxDate") ? null : obj.getString("maxDate");
            if (maxDateString != null && maxDateString.length() > 0) {
                _maxDateCalendar = GregorianCalendar.getInstance();
                _maxDateCalendar.setTime(_dateFormat.parse(maxDateString));
            }

            _doneButtonLabel = obj.getString("doneButtonLabel");
            _cancelButtonLabel = obj.getString("cancelButtonLabel");
        }
        catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        catch (ParseException e) {
            e.printStackTrace();
            return false;
        }

        _callbackContext = callbackContext;
        _cancelled = false;

        show();

        return true;
    }

    private synchronized void show() {
        final Context currentContext = cordova.getActivity();
        final Runnable runnable;

        if (_mode.equalsIgnoreCase(MODE_DATE)) {
            runnable = new Runnable() {
                @Override
                public void run() {
                    final DateSetListener dateSetListener = new DateSetListener(SyrecDatePicker.this);
                    final DatePickerDialog dateDialog = new DatePickerDialog(currentContext, dateSetListener,
                            _dateCalendar.get(Calendar.YEAR),
                            _dateCalendar.get(Calendar.MONTH),
                            _dateCalendar.get(Calendar.DAY_OF_MONTH));

                    DatePicker dp = dateDialog.getDatePicker();

                    if (dp != null) {
                        if (_minDateCalendar != null) {
                            dp.setMinDate(_minDateCalendar.getTimeInMillis());
                        }

                        if (_maxDateCalendar != null && (_minDateCalendar == null || _maxDateCalendar.compareTo(_minDateCalendar) > 0)) {
                            dp.setMaxDate(_maxDateCalendar.getTimeInMillis());
                        }
                    }

                    dateDialog.setCancelable(true);
                    dateDialog.setCanceledOnTouchOutside(false);

                    Button doneButton = dateDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    if (doneButton != null) {
                        doneButton.setText(_doneButtonLabel);
                    }

                    dateDialog.setButton(DialogInterface.BUTTON_NEGATIVE, _cancelButtonLabel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            cancel();
                            dialog.dismiss();
                        }
                    });

                    dateDialog.show();
                }
            };
        }
        else if (_mode.equalsIgnoreCase(MODE_TIME)) {
            runnable = new Runnable() {
                @Override
                public void run() {
                    final TimeSetListener timeSetListener = new TimeSetListener(SyrecDatePicker.this);
                    final TimePickerDialog timeDialog = new TimePickerDialog(currentContext, timeSetListener,
                            _dateCalendar.get(Calendar.HOUR_OF_DAY),
                            _dateCalendar.get(Calendar.MINUTE),
                            true);

                    timeDialog.setCancelable(true);
                    timeDialog.setCanceledOnTouchOutside(false);

                    Button doneButton = timeDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    if (doneButton != null) {
                        doneButton.setText(_doneButtonLabel);
                    }

                    timeDialog.setButton(DialogInterface.BUTTON_NEGATIVE, _cancelButtonLabel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            cancel();
                            dialog.dismiss();
                        }
                    });

                    timeDialog.show();
                }
            };
        }
        else if (_mode.equalsIgnoreCase(MODE_DATETIME)) {
            runnable = new Runnable() {
                @Override
                public void run() {
                    final Dialog dateTimeDialog = new Dialog(currentContext);

                    dateTimeDialog.setContentView(R.layout.date_time_layout);
                    dateTimeDialog.setCancelable(true);
                    dateTimeDialog.setCanceledOnTouchOutside(false);

                    DatePicker dp = (DatePicker) dateTimeDialog.findViewById(R.id.datePicker);
                    if (dp != null) {
                        dp.init(_dateCalendar.get(Calendar.YEAR),
                                _dateCalendar.get(Calendar.MONTH),
                                _dateCalendar.get(Calendar.DAY_OF_MONTH),
                                new DatePicker.OnDateChangedListener() {
                                    @Override
                                    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                        setDate(year, monthOfYear, dayOfMonth);
                                    }
                                }
                        );

                        if (_minDateCalendar != null) {
                            dp.setMinDate(_minDateCalendar.getTimeInMillis());
                        }

                        if (_maxDateCalendar != null && (_minDateCalendar == null || _maxDateCalendar.compareTo(_minDateCalendar) > 0)) {
                            dp.setMaxDate(_maxDateCalendar.getTimeInMillis());
                        }
                    }

                    TimePicker tp = (TimePicker) dateTimeDialog.findViewById(R.id.timePicker);
                    if (tp != null) {
                        tp.setCurrentHour(_dateCalendar.get(Calendar.HOUR_OF_DAY));
                        tp.setCurrentMinute(_dateCalendar.get(Calendar.MINUTE));
                        tp.setIs24HourView(true);
                        tp.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
                            @Override
                            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                                setTime(hourOfDay, minute);
                            }
                        });
                    }

                    Button doneButton = (Button) dateTimeDialog.findViewById(R.id.doneButton);
                    if (doneButton != null) {
                        doneButton.setText(_doneButtonLabel);
                        doneButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                done();
                                dateTimeDialog.dismiss();
                            }
                        });
                    }

                    Button cancelButton = (Button) dateTimeDialog.findViewById(R.id.cancelButton);
                    if (cancelButton != null) {
                        cancelButton.setText(_cancelButtonLabel);
                        cancelButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                cancel();
                                dateTimeDialog.dismiss();
                            }
                        });
                    }

                    dateTimeDialog.show();
                }
            };
        }
        else {
            Log.d(TAG, "Unknown mode. Only 'date', 'time' or 'datetime' are allowed");
            return;
        }

        cordova.getActivity().runOnUiThread(runnable);
    }

    private void setDate(int year, int monthOfYear, int dayOfMonth) {
        _dateCalendar.set(Calendar.YEAR, year);
        _dateCalendar.set(Calendar.MONTH, monthOfYear);
        _dateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
    }

    private void setTime(int hourOfDay, int minute) {
        _dateCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        _dateCalendar.set(Calendar.MINUTE, minute);
    }

    private void done() {
        String result = _dateFormat.format(_dateCalendar.getTime());
        _callbackContext.success(result);
    }

    private void cancel() {
        _cancelled = true;
        _callbackContext.error(0);
    }

    private boolean isCancelled() {
        return _cancelled;
    }

    private final class DateSetListener implements OnDateSetListener {
        private final SyrecDatePicker _syrecDatePicker;

        private DateSetListener(SyrecDatePicker syrecDatePicker) {
            _syrecDatePicker = syrecDatePicker;
        }

        @Override
        public void onDateSet(final DatePicker view, final int year, final int monthOfYear, final int dayOfMonth) {
            if (!_syrecDatePicker.isCancelled()) {
                _syrecDatePicker.setDate(year, monthOfYear, dayOfMonth);
                _syrecDatePicker.done();
            }
        }
    }

    private final class TimeSetListener implements OnTimeSetListener {
        private final SyrecDatePicker _syrecDataPicker;

        private TimeSetListener(SyrecDatePicker syrecDatePicker) {
            _syrecDataPicker = syrecDatePicker;
        }

        @Override
        public void onTimeSet(final TimePicker view, final int hourOfDay, final int minute) {
            if (!_syrecDataPicker.isCancelled()) {
                _syrecDataPicker.setTime(hourOfDay, minute);
                _syrecDataPicker.done();
            }
        }
    }
}
