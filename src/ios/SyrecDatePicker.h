/*
  Cordova DatePicker Plugin
  https://github.com/syreclabs/syrec-plugins-datepicker.git
*/

#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>

@interface SyrecDatePicker : CDVPlugin

- (void)show:(CDVInvokedUrlCommand *)command;

@end
