/**
  Cordova DatePicker Plugin
  https://github.com/syreclabs/syrec-plugins-datepicker.git
*/

var exec = require('cordova/exec');

var DatePicker = function() {
};

DatePicker.show = function(options, cb) {
  function parseDate(arg) {
    if (!arg)
      return null;

    var date = null;

    if (typeof arg === 'object') {
      // Date instance
      date = arg;
    }
    else if (typeof arg === 'number') {
      // timestamp
      date = new Date(arg);
    }
    else if (typeof arg === 'string') {
      // parseable Date string
      date = Date.parse(arg);
    }

    if (date === null)
      throw('Could not parse date: ' + arg);
    else
      return date.toISOString();
  }

  options.date = parseDate(options.date);
  options.minDate = parseDate(options.minDate);
  options.maxDate = parseDate(options.maxDate);

  var args = {
    mode: 'date',
    date: new Date(),
    minDate: null,
    maxDate: null,
    doneButtonLabel: 'Done',
    // doneButtonColor: '#0000FF',
    cancelButtonLabel: 'Cancel',
    // cancelButtonColor: '#000000',
    x: '0',
    y: '0'
  };

  for (var key in args) {
    if (typeof options[key] !== 'undefined')
      args[key] = options[key];
  }

  var success = function(date) {
    if (cb) {
      cb(new Date(date));
    }
  };

  exec(success, null, 'DatePicker', 'show', [args]);
};

module.exports = DatePicker;
